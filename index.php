<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width =device-width, initial-scale =1.0, user-scalable =yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="UTF-8">

    <title>Youtube</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />

    <!-- <link href="css/estilos.css" rel="stylesheet" /> -->
    <!-- <link rel="stylesheet" href="css/estilos__2.css" /> -->
    <link rel="stylesheet" href="css/estilos__generales.css" />
    <link rel="stylesheet" href="css/estilos__index.css" />

    <!-- <link rel="stylesheet" href="css/estilos__input.css" /> -->

    
</head>
<body>

<?php
    include("include/menu.php");
?>

<?php
    include("include/header.php");
 ?>

     <div class="contenedor__inicio">
    
        <div class="ir__video">
            <a href="video.php">Ir a secciòn video</a>
        </div>

        <div class="titulo__inicio">
            <h2>Inicio</h2>
        </div>

        <div class="contenedor__videos">
 
            <div class="elemento__video">
                <a href="video.php">                
                    <img src="img/img__video5.jpg" alt=""> 
                    
                    <div class="video__usuario">                        
                        <a href="#" class="video__usuario">
                            <img src="img/img__video5.jpg" alt="">       
                        </a> 

                        <div class="video__datos__video">   
                            <a href="#">

                                <h3 class="video--titulo">Titulo video</h3>                 
                                <p class ="usuario--item">Usuario</p> 
                                <p class ="visualizaciones--item">2000 Visualizaciones</p> 
                                <p class ="subido--item">Subido</p> 
                            </a>                                         
                        </div>
                    </div>
                </a>
    
            </div>

            <div class="elemento__video">
                <a href="video.php">
                    <img src="img/img__video2.jpg" alt=""> 
                    
                    <div class="video__usuario">                        
                        <a href="#" class="video__usuario">
                            <img src="img/img__video2.jpg" alt="">       
                        </a> 

                        <div class="video__datos__video">   
                            <a href="#">

                                <h3 class="video--titulo">Titulo video</h3>                 
                                <p class ="usuario--item">Usuario</p> 
                                <p class ="visualizaciones--item">2000 Visualizaciones</p> 
                                <p class ="subido--item">Subido</p> 
                            </a>                                         
                        </div>
                    </div>
                </a>
    
            </div>

            <div class="elemento__video">
                <a href="video.php">
                    <img src="img/img__video3.jpg" alt=""> 
                    
                    <div class="video__usuario">                        
                        <a href="#" class="video__usuario">
                            <img src="img/img__video3.jpg" alt="">       
                        </a> 

                        <div class="video__datos__video">   
                            <a href="#">

                                <h3 class="video--titulo">Titulo video</h3>                 
                                <p class ="usuario--item">Usuario</p> 
                                <p class ="visualizaciones--item">2000 Visualizaciones</p> 
                                <p class ="subido--item">Subido</p> 
                            </a>                                         
                        </div>
                    </div>
                </a>
    
            </div>

            <div class="elemento__video">
                <a href="video.php">
                    <img src="img/img__video4.jpg" alt=""> 
                    
                    <div class="video__usuario">                        
                        <a href="#" class="video__usuario">
                            <img src="img/img__video4.jpg" alt="">       
                        </a> 

                        <div class="video__datos__video">   
                            <a href="#">

                                <h3 class="video--titulo">Titulo video</h3>                 
                                <p class ="usuario--item">Usuario</p> 
                                <p class ="visualizaciones--item">2000 Visualizaciones</p> 
                                <p class ="subido--item">Subido</p> 
                            </a>                                         
                        </div>
                    </div>
                </a>
    
            </div>

 

 

            
        </div>

     </div>

     
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
</body>
</html>