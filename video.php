<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width =device-width, initial-scale =1.0, user-scalable =yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="UTF-8">

    <title>Youtube</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link href="css/estilos.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/estilos__2.css" />
</head>
<body>

<?php
    include("include/header.php");
 ?>

<?php
    include("include/video.php");
 ?>

<?php
    include("include/video__siguiente.php")
 ?>
    
</body>
</html>