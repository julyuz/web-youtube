<!-- <link rel="stylesheet" href="css/estilos__generales.css" /> -->
<link rel="stylesheet" href="css/estilos__header.css" />

<header id ="header">
    <div class="flex-container__header">

        <div class="header-izquierdo">        
            <!-- <label class="label--check" for="check"><span class="icono--fa"><i class="fa fa-bars fa-lg"></i></span></label> -->
            <!-- <label class="label--check" for="check__menu"> -->

            <div id="mostrar-nav">
                <img id="" src="img/menu (1).png" alt="" id="icono-menu">
                <a href="index.php" id ="enlace__logo--youtube">
                    <img id="logo__youtube" src="img/youtube.png" alt="logo youtube" />
                    <h2 id="titulo">YouTube</h2>
                </a>  
            </div>


                <!-- <img src="img/menu (1).png" alt="" id="icono-menu"> -->
            <!-- </label> -->
            <!-- <input type="checkbox" id="check__menu"/> -->
            <!-- <a href="index.php" id ="enlace__logo--youtube">
                <img id="logo__youtube" src="img/youtube.png" alt="logo youtube" />
                <h2 id="titulo">YouTube</h2>
            </a>   -->

        </div>

        <div class="header-derecho">                
            <div id="buscar">
                <div class="inputWithIcon inputIconBg">
                    <input type="text" placeholder="Buscar">
                    <button id = "buscar__btn"><i class="fa fa-search fa-md fa-fw" aria-hidden="true"></i></span></button> 
                </div>
            </div>

            <div class="bloque__iconos__avatar">
                <button id = "subir__btn"><i class="fa fa-upload fa-lg fa-fw" aria-hidden="true"></i></button> 
                <button id = "notificaciones__btn"><i class="fa fa-bell fa-lg fa-fw" aria-hidden="true"></i></button> 
                <img src="img/peje.jpg" alt="" id="usuario__img">
            </div>
        </div>

    </div>
</header>