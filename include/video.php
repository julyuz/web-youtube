<?php
    include('include/menu.php');
 ?>

<section class="video">
        <div class="flex-container__video">
            <img class="video__img" src="img/kakr.jpg" alt="">
            <!-- <img class="video__img" src="img/bg_video.jpg" alt=""> -->
        </div>

        <div class="flex-container__video--titulo">    
            <h3 class="video--titulo">Titulo video</h3>
        </div>

        <div class="flex-container__video--iconos">
            <div class="iconos__visualizaciones">
                <p class ="visualizaciones--item">2000 Visualizaciones</p>
            </div>   

            <div class="iconos__gusta">
                <div class="iconos__gusta--gusta">
                    <span class ="icono--fa ">
                        <i class="fa fa-thumbs-up fa-lg"></i>                    
                    </span >
                    <p class ="iconos__gusta--item">10</p>
                </div>

                <div class="iconos__gusta--nogusta">
                    <span class ="icono--fa ">
                        <i class="fa fa-thumbs-down fa-lg"></i>
                    </span >
                    <p class ="iconos__gusta--item">5</p>
                </div>
            </div>

            <div class="iconos__compartir__guardar">
                <div class="iconos__compartir__guardar--compartir">
                    <span class ="icono--fa" >
                        <i class="fa fa-share fa-1x"></i>
                    </span >
                    <p class ="iconos__compartir__guardar--item">Compartir</p>
                </div>

                <div class="iconos__compartir__guardar--guardar">
                    <span class ="icono--fa" >
                        <i class="fa fa-plus-square fa-1x"></i>
                    </span >                
                    <p class ="iconos__compartir__guardar--item">Guardar</p>
                </div>
            </div>

            <div class="iconos__menu">
                <input id ="check__id" class ="check__submenu" type="checkbox">
                <span class="icono--fa iconos__menu--item">
                    <label for="check__id">
                        <i class="fa fa-ellipsis-h fa-lg"></i>
                    </label>
                </span>

                <ul class="iconos__menu--submenu">
                    <li class ="iconos__menu--submenu__item"><a class ="iconos__menu--submenu__item--enlace" href="#">Denunciar</a></li>
                    <li class ="iconos__menu--submenu__item"><a class ="iconos__menu--submenu__item--enlace" href="#">Abrir la transcripción</a></li>
                    <li class ="iconos__menu--submenu__item"><a class ="iconos__menu--submenu__item--enlace" href="#">Añadir traducciones</a></li>
                </ul>

            </div>            
        </div>

        <div class="flex-container__video--info">

            <div class="info__usuario__suscripciones">

                <div class ="info__usuario">
                    <div class="info__usuario--icono">
                        <img src="img/dach.jpg" alt="usuario--icono">
                    </div>

                    <div class="info__usuario--nombre">
                        <p class ="info__usuario--nombre__nombre">Julio Alvarado</p>
                        <p class="info__usuario--nombre__fecha--publicado">Publicado el 11 Nov 2018</p>
                    </div>
                </div>
                
                <div class="info__suscripciones">
                    
                    <a href"#">Suscrito 10 Mil</a>
                    <div class="info__suscripciones--campana">
                        <span class ="icono--fa">
                            <i class="fa fa-bell fa-lg"></i>
                        </span>
                    </div>
                </div>
            </div>

            
            <div class="info__descripcion">
                <p>Descripción</p>
            </div>
    
            <div class="info__mostrar--mas">
                <a href="#">Mostrar más</a>
            </div>


        </div>

    </section>