<section class="video__siguiente">
    <div class="siguiente__repr__auto">       

        <div class="siguiente">
            <p>Siguiente</p>
        </div>

        <div class="repr__auto">
            <label >Reproducción automática</label>
            
            <div class="repr__auto--checkbox">                
                <input id="check" type="checkbox">
                <label id="label__oculto" for="check"></label>
            </div>
        </div>
    </div>

    <div class="siguiente__video__titulo">

            <div class="siguiente__video">
                <a href="#" class ="enlace__video">
                    <div class="siguiente__video--img">
                        <img src="img/img__video.jpg" alt="">
                    </div>
                </a>
            </div>

            <div class="siguiente__titulo">

                <a href="#" class ="enlace__video">
                    <div class="siguiente__titulo--titulo">
                        <h3 class="video--titulo">Titulo video</h3>
                    </div>

                    <div class="siguiente__titulo--usuario">
                        <p class ="usuario--item">Usuario</p>
                    </div>

                    <div class="siguiente__titulo--visualizaciones">
                        <p class ="visualizaciones--item">100k visualizaciones</p>
                    </div>
                </a>
            </div>
    </div>

    <div class="video__titulo id video__titulo--1">
        <div class="__video">
            <a href="#" class ="enlace__video">
                <div class="__video--img">
                    <img src="img/img__video1.jpg" alt="">
                </div>
            </a>
        </div>

        <div class="__titulo">
            <a href="#" class ="enlace__video">
                <div class="__titulo--titulo">
                    <h3 class="video--titulo">Titulo video</h3>
                </div>

                <div class="__titulo--usuario">
                    <p class ="usuario--item">Usuario</p>
                </div>

                <div class="__titulo--visualizaciones">
                    <p class ="visualizaciones--item">100k visualizaciones</p>
                </div>
            </a>
        </div>

    </div>

    <div class="video__titulo">
        <div class="__video">
            <a href="#" class ="enlace__video">
                <div class="__video--img">
                    <img src="img/img__video3.jpg" alt="">
                </div>
            </a>
        </div>

        <div class="__titulo">
            <a href="#" class ="enlace__video">
                <div class="__titulo--titulo">
                    <h3 class="video--titulo">Titulo video</h3>
                </div>

                <div class="__titulo--usuario">
                    <p class ="usuario--item">Usuario</p>
                </div>

                <div class="__titulo--visualizaciones">
                    <p class ="visualizaciones--item">100k visualizaciones</p>
                </div>
            </a>
        </div>

    </div>

    <div class="video__titulo">
        <div class="__video">
            <a href="#" class ="enlace__video">
                <div class="__video--img">
                    <img src="img/img__video2.jpg" alt="">
                </div>
            </a>
        </div>

        <div class="__titulo">
            <a href="#" class ="enlace__video">
                <div class="__titulo--titulo">
                    <h3 class="video--titulo">Titulo video</h3>
                </div>

                <div class="__titulo--usuario">
                    <p class ="usuario--item">Usuario</p>
                </div>

                <div class="__titulo--visualizaciones">
                    <p class ="visualizaciones--item">100k visualizaciones</p>
                </div>
            </a>
        </div>

    </div>

</section>